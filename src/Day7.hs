module Day7 (part1, part2) where

import Data.Bifunctor (first)
import Data.Function (on)
import Data.List (find, group, nub, partition, sort, sortBy)
import Data.Maybe (fromJust)
import Util

data Card = Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten | Jack | Queen | King | Ace
  deriving (Show, Eq, Ord, Enum, Bounded)

newtype Hand = Hand {toCards :: [Card]} deriving (Show, Eq)
newtype JokerHand = JokerHand {toJokerCards :: [Card]} deriving (Show, Eq)

data HandType = HighCard | OnePair | TwoPair | ThreeOfAKind | FullHouse | FourOfAKind | FiveOfAKind
  deriving (Show, Eq, Ord, Enum, Bounded)

instance Ord Hand where
  compare h1 h2 =
    let
      ht1 = handType h1
      ht2 = handType h2
     in
      if ht1 == ht2
        then fromJust . find (/= EQ) $ zipWith compare (toCards h1) (toCards h2)
        else compare ht1 ht2

instance Ord JokerHand where
  compare h1 h2 =
    let
      ht1 = jokerHandType h1
      ht2 = jokerHandType h2

      compareJokerCard c1 c2 =
        case (c1, c2) of
          (Jack, Jack) -> EQ
          (Jack, _) -> LT
          (_, Jack) -> GT
          _ -> compare c1 c2
     in
      if ht1 == ht2
        then
          fromJust . find (/= EQ) $
            zipWith compareJokerCard (toJokerCards h1) (toJokerCards h2)
        else compare ht1 ht2

part1 :: IO ()
part1 = solveLinesWithParser "test/Day7/input" parseHandAndBid solveHands

part2 :: IO ()
part2 = solveLinesWithParser "test/Day7/input" (first toJokerHand <$> parseHandAndBid) solveHands

solveHands :: (Ord a) => [(a, Int)] -> Int
solveHands = sum . zipWith (\rank (_, bid) -> rank * bid) [1 ..] . sortBy (compare `on` fst)

handType :: Hand -> HandType
handType (Hand hand)
  | 5 `elem` lengths = FiveOfAKind
  | 4 `elem` lengths = FourOfAKind
  | 2 `elem` lengths && 3 `elem` lengths = FullHouse
  | 3 `elem` lengths = ThreeOfAKind
  | length (filter (== 2) lengths) == 2 = TwoPair
  | 2 `elem` lengths = OnePair
  | otherwise = HighCard
  where
    lengths = map length . group . sort $ hand

jokerHandType :: JokerHand -> HandType
jokerHandType (JokerHand hand) =
  let
    (jokers, nonJokers) = partition (== Jack) hand
    mkHandFromJokers c =
      handType . Hand $ map (const c) jokers ++ nonJokers
   in
    case (jokers, nonJokers) of
      ([], _) -> handType (Hand hand)
      (_, []) -> FiveOfAKind
      _ -> maximum . map mkHandFromJokers $ nub nonJokers

toJokerHand :: Hand -> JokerHand
toJokerHand (Hand hand) = JokerHand hand

parseHandAndBid :: Parser (Hand, Int)
parseHandAndBid =
  (,) . Hand . map charToCard <$> many (anySingleBut ' ') <* char ' ' <*> decimal

charToCard :: Char -> Card
charToCard c =
  case c of
    'A' -> Ace
    'K' -> King
    'Q' -> Queen
    'J' -> Jack
    'T' -> Ten
    '9' -> Nine
    '8' -> Eight
    '7' -> Seven
    '6' -> Six
    '5' -> Five
    '4' -> Four
    '3' -> Three
    '2' -> Two
    _ -> error $ "Bad card type: " ++ [c]
