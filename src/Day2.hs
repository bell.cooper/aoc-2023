module Day2 (part1, part2) where

import Data.List (foldl')
import Data.Maybe (fromJust)
import Util

data Color = Red Int | Blue Int | Green Int
  deriving (Eq, Show)

type Round = [Color]

data Game = Game
  { gameId :: Int
  , rounds :: [Round]
  }
  deriving (Show)

part1 :: IO ()
part1 = solveLines "test/Day2/input" $ sumPossibleGameIds . parseGames

part2 :: IO ()
part2 = solveLines "test/Day2/input" $ sum . map sumPowerofCubeSets . parseGames

sumPowerofCubeSets :: Game -> Int
sumPowerofCubeSets game =
  let
    colors = concat . rounds $ game
    maxColor isColor = maximum . map colorNum . filter isColor $ colors
   in
    maxColor isBlue * maxColor isGreen * maxColor isRed

colorNum :: Color -> Int
colorNum (Blue n) = n
colorNum (Green n) = n
colorNum (Red n) = n

isBlue :: Color -> Bool
isBlue (Blue _) = True
isBlue _ = False

isGreen :: Color -> Bool
isGreen (Green _) = True
isGreen _ = False

isRed :: Color -> Bool
isRed (Red _) = True
isRed _ = False

sumPossibleGameIds :: [Game] -> Int
sumPossibleGameIds =
  foldl' (\a g -> a + gameId g) 0
    . filter (not . any impossibleGame . rounds)

impossibleGame :: Round -> Bool
impossibleGame rnd =
  let
    impossible (Red n) = n > 12
    impossible (Green n) = n > 13
    impossible (Blue n) = n > 14
   in
    any impossible rnd

parseGames :: [String] -> [Game]
parseGames = fromJust . traverse (parseMaybe parseGame)

parseGame :: Parser Game
parseGame = do
  Game <$> parseGameId <*> sepBy parseRound (chunk "; ")

parseGameId :: Parser Int
parseGameId = chunk "Game " *> decimal <* chunk ": "

parseRound :: Parser Round
parseRound =
  (try parseRed <|> try parseBlue <|> parseGreen) `sepBy` chunk ", "

parseRed :: Parser Color
parseRed = Red <$> decimal <* chunk " red"

parseBlue :: Parser Color
parseBlue = Blue <$> decimal <* chunk " blue"

parseGreen :: Parser Color
parseGreen = Green <$> decimal <* chunk " green"
