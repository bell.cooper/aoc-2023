module Day9 (part1, part2) where

import Util

part1 :: IO ()
part1 =
  solveLinesWith "test/Day9/input" (map (read @Int) . words) $
    sum . map (sum . map last . allDiffs)

part2 :: IO ()
part2 =
  solveLinesWith "test/Day9/input" (map (read @Int) . words) $
    sum . map (foldr ((-) . head) 0 . allDiffs)

allDiffs :: [Int] -> [[Int]]
allDiffs = takeWhile (any (/= 0)) . iterate diffs

diffs :: [Int] -> [Int]
diffs (x : y : rs) = (y - x) : diffs (y : rs)
diffs _ = []
