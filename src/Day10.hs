module Day10 (part1, part2) where

import Data.Function (on)
import Data.List (maximumBy, nub, partition, sortBy)
import qualified Data.Map.Strict as M
import Data.Maybe (mapMaybe)
import qualified Data.Set as S
import Util

import Debug.Trace

part1 :: IO ()
part1 =
  solveGrid "test/Day10/input" $
    snd . maximumBy (compare `on` snd) . uncurry bfs . gridToGraph

part2 :: IO ()
part2 = solveGrid "test/Day10/sample2" $ \grid ->
  let (start, graph) = gridToGraph grid
      pts = nub . map fst $ bfs start graph
   in filter (\n@(_, c) -> c == '.' && canEscape grid (At n)) $ gridToList grid

type GridCoord a = (Coord, a)

data Search a = InBetween (GridCoord a) (GridCoord a) | At (GridCoord a)
  deriving (Show, Eq)

verticalSqueezeShapes :: [(Char, Char)]
verticalSqueezeShapes =
  [ ('|', '|')
  , ('|', 'L')
  , ('J', '|')
  , ('J', 'L')
  , ('J', 'F')
  , ('7', 'L')
  , ('7', '|')
  , ('7', 'F')
  , ('|', 'F')
  ]

horizontalSqueezeShapes :: [(Char, Char)]
horizontalSqueezeShapes =
  [ ('-', '-')
  , ('-', '7')
  , ('-', 'F')
  , ('L', '-')
  , ('J', '-')
  , ('J', 'F')
  , ('J', '7')
  , ('L', 'F')
  , ('L', '7')
  ]

canEscape :: Grid Char -> Search Char -> Bool
canEscape grid (InBetween ((x1, y1), _) ((x2, y2), _))
  | x1 == x2 && (x1 - 1 < 0 || x1 + 1 >= gridWidth grid) = True
  | y1 == y2 && (y1 - 1 < 0 || y1 + 1 >= gridHeight grid) = True
  | x1 == x2 =
      let c1r = grid <!> (x1 + 1, y1)
          c1l = grid <!> (x1 - 1, y1)
          c2r = grid <!> (x2 + 1, y2)
          c2l = grid <!> (x2 - 1, y2)
       in any
            (canEscape grid)
            [ InBetween ((x1 + 1, y1), c1r) ((x1 - 1, y1), c1l)
            , InBetween ((x2 + 1, y2), c2r) ((x2 - 1, y2), c2l)
            ]
  | y1 == y2 =
      let c1d = grid <!> (x1, y1 + 1)
          c1u = grid <!> (x1, y1 - 1)
          c2d = grid <!> (x2, y2 + 1)
          c2u = grid <!> (x2, y2 - 1)
       in any
            (canEscape grid)
            [ InBetween ((x1, y1 + 1), c1d) ((x1, y1 - 1), c1u)
            , InBetween ((x2, y2 + 1), c2d) ((x2, y2 - 1), c2u)
            ]
canEscape grid (At (coord, _)) =
  let ns = neighbors coord grid
      outsidePts =
        filter (\((x, y), _) -> x < 0 || x >= gridWidth grid || y < 0 || y >= gridHeight grid) ns
      (groundPts, pipePts) = partition ((== '.') . snd) ns

      groupNeighbors [] = []
      groupNeighbors (n1@((x1, y1), _) : pts) =
        groupNeighbors pts
          ++ [ (n1, n2) | n2@((x2, y2), _) <- pts, x1 == x2 + 1 && y1 == y2
                                                    || y1 == y2 + 1 && x1 == x2
             ]

      maybeInBetween n1@((x1, y1), a1) n2@((x2, y2), a2) =
        if (x1 == x2 && (a1, a2) `elem` verticalSqueezeShapes)
          || (y1 == y2 && (a1, a2) `elem` horizontalSqueezeShapes)
          then Just (InBetween n1 n2)
          else Nothing
   in not (null outsidePts)
        && ( any (canEscape grid . At) groundPts
              || any (canEscape grid) (mapMaybe (uncurry maybeInBetween) (groupNeighbors pipePts))
           )

bfs :: (Show a, Ord a) => a -> M.Map a [a] -> [(a, Int)]
bfs start adjList =
  let
    loop queue visited =
      case queue of
        [] -> []
        ((node, d) : rest) ->
          let
            adjElems = filter (`S.notMember` visited) $ adjList M.! node
            newVisited = S.insert node visited
           in
            (node, d) : loop (rest ++ map (,d + 1) adjElems) newVisited
   in
    loop [(start, 0)] S.empty

gridToGraph :: Grid Char -> (Coord, M.Map Coord [Coord])
gridToGraph grid =
  let
    north (x, y) = (x, y - 1)
    south (x, y) = (x, y + 1)
    east (x, y) = (x + 1, y)
    west (x, y) = (x - 1, y)

    f (start, graph) coord pipeChar =
      case pipeChar of
        '|' -> (start, M.insert coord [north coord, south coord] graph)
        '-' -> (start, M.insert coord [east coord, west coord] graph)
        'L' -> (start, M.insert coord [north coord, east coord] graph)
        'J' -> (start, M.insert coord [north coord, west coord] graph)
        '7' -> (start, M.insert coord [south coord, west coord] graph)
        'F' -> (start, M.insert coord [south coord, east coord] graph)
        'S' -> (Just coord, graph)
        _ -> (start, graph)

    (Just s, finalGraph) = ifoldGridl f (Nothing, M.empty) grid

    startNeighbors = M.keys $ M.filter (s `elem`) finalGraph
   in
    (s, M.insert s startNeighbors finalGraph)
