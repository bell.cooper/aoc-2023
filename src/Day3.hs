module Day3 (part1, part2) where

import qualified Data.Char as Char
import qualified Data.Map.Strict as M
import Data.Maybe (mapMaybe)
import qualified Data.Set as S
import Util

part1 :: IO ()
part1 = solveGridWith "test/Day3/input" toCell $ \grid ->
  let
    adjLists =
      finishNum $
        ifoldGridl buildAdjLists (AdjLists M.empty M.empty Nothing []) grid

    numberSet i =
      foldr
        ( \(neighbor, _) s ->
            case M.lookup neighbor (numbers adjLists) of
              Nothing -> s
              Just n -> S.insert n s
        )
        S.empty
        (neighbors i grid)
   in
    sum . map fst . S.toList $
      foldMap numberSet (M.keys $ symbols adjLists)

part2 :: IO ()
part2 = solveGridWith "test/Day3/input" toCell $ \grid ->
  let
    adjLists =
      finishNum $
        ifoldGridl buildAdjLists (AdjLists M.empty M.empty Nothing []) grid

    gearRatios i sym =
      if sym == '*'
        then
          let
            ns =
              foldr
                ( \(neighbor, _) s ->
                    case M.lookup neighbor (numbers adjLists) of
                      Nothing -> s
                      Just n -> S.insert n s
                )
                S.empty
                (neighbors i grid)
           in
            case S.toList ns of
              [(f, _), (s, _)] -> Just (f * s)
              _ -> Nothing
        else Nothing
   in
    sum . mapMaybe (uncurry gearRatios) . M.toList $ symbols adjLists

buildAdjLists :: AdjLists -> (Int, Int) -> Cell -> AdjLists
buildAdjLists lst i cell =
  case cell of
    Dot -> finishNum lst
    Num n -> addNum n i lst
    Symbol s -> addSymbol s i lst

finishNum :: AdjLists -> AdjLists
finishNum lst =
  case currentNum lst of
    Nothing -> lst
    Just n ->
      lst
        { numbers =
            foldr
              (\i m -> M.insert i (n, currentNumPos lst) m)
              (numbers lst)
              (currentNumPos lst)
        , currentNum = Nothing
        , currentNumPos = []
        }

addNum :: Int -> (Int, Int) -> AdjLists -> AdjLists
addNum n i lst =
  lst
    { currentNum = Just $ maybe n (\n' -> 10 * n' + n) (currentNum lst)
    , currentNumPos = i : currentNumPos lst
    }

addSymbol :: Char -> (Int, Int) -> AdjLists -> AdjLists
addSymbol s i lst = finishNum $ lst {symbols = M.insert i s (symbols lst)}

data AdjLists = AdjLists
  { numbers :: M.Map (Int, Int) (Int, [(Int, Int)])
  , symbols :: M.Map (Int, Int) Char
  , currentNum :: Maybe Int
  , currentNumPos :: [(Int, Int)]
  }
  deriving (Show)

data Cell = Dot | Num Int | Symbol Char
  deriving (Eq, Show)

toCell :: Char -> Cell
toCell cell =
  case cell of
    '.' -> Dot
    c | Char.isDigit c -> Num (Char.ord c - Char.ord '0')
    c -> Symbol c
