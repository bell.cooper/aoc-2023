module Main (main) where

import qualified Day1
import qualified Day2
import qualified Day3
import qualified Day4
import qualified Day5
import qualified Day6
import qualified Day7
import qualified Day8
import qualified Day9

printSolutions :: [IO ()] -> IO ()
printSolutions sln =
  let
    go (part1, part2) day =
      [ putStrLn $ "Day " ++ show day
      , putStr "part1: "
      , part1
      , putStr "part2: "
      , part2
      , putStrLn ""
      ]

    pairSoln [] = []
    pairSoln (x : y : rst) = (x, y) : pairSoln rst
   in
    sequence_ $
      foldr (\(d, s) a -> go s d ++ a) [] $
        zip [1 ..] (pairSoln sln)

main :: IO ()
main =
  printSolutions
    [ Day1.part1
    , Day1.part2
    , Day2.part1
    , Day2.part2
    , Day3.part1
    , Day3.part2
    , Day4.part1
    , Day4.part2
    , Day5.part1
    , Day5.part2
    , Day6.part1
    , Day6.part2
    , Day7.part1
    , Day7.part2
    , Day8.part1
    , Day8.part2
    , Day9.part1
    , Day9.part2
    ]
