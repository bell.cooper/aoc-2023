module Util
  ( solve
  , solveLines
  , solveLinesWith
  , solveWithParser
  , solveLinesWithParser
  , solveGrid
  , solveGridWith
  , Coord
  , toGrid
  , Grid
  , gridToList
  , gridWidth
  , gridHeight
  , gridDimensions
  , gridCells
  , gridLookup
  , (<!>)
  , ifoldGridl
  , neighbors
  -- Megaparsec
  , Parser
  , module Parsec
  , rotateR
  , flipRows
  , transpose
  )
where

import Control.Exception (throw)
import Data.List (foldl')
import qualified Data.Vector as V
import Data.Void
import Text.Megaparsec as Parsec
import Text.Megaparsec.Char as Parsec
import Text.Megaparsec.Char.Lexer as Parsec hiding (space)

type Parser = Parsec Void String

solve :: (Show a) => FilePath -> (String -> a) -> IO ()
solve fileName solution = do
  file <- readFile fileName
  print (solution file)

solveLines :: (Show a) => FilePath -> ([String] -> a) -> IO ()
solveLines fileName solution = solve fileName (solution . lines)

solveLinesWith ::
  (Show a) =>
  FilePath ->
  (String -> b) ->
  ([b] -> a) ->
  IO ()
solveLinesWith filePath f solution = solveLines filePath (solution . map f)

solveLinesWithParser ::
  (Show a) =>
  FilePath ->
  Parser b ->
  ([b] -> a) ->
  IO ()
solveLinesWithParser filePath p solution =
  let
    parseLine l =
      case runParser p "" l of
        Left e -> throw e
        Right r -> r
   in
    solveLines filePath (solution . map parseLine)

solveWithParser ::
  (Show a) =>
  FilePath ->
  Parser b ->
  (b -> a) ->
  IO ()
solveWithParser filePath p solution =
  let
    parseInput l =
      case runParser p "" l of
        Left e -> throw e
        Right r -> r
   in
    solve filePath (solution . parseInput)

solveGrid :: (Show a) => FilePath -> (Grid Char -> a) -> IO ()
solveGrid fileName solution = solve fileName (solution . toGrid)

toGrid :: String -> Grid Char
toGrid input =
  let ls = lines input
   in Grid (length (head ls)) (length ls) (V.fromList $ concat ls)

solveGridWith ::
  (Show a) =>
  FilePath ->
  (Char -> b) ->
  (Grid b -> a) ->
  IO ()
solveGridWith fileName p solution =
  solveGrid fileName (solution . fmap p)

data Grid a = Grid
  { gridWidth :: Int
  , gridHeight :: Int
  , gridCells :: V.Vector a
  }
  deriving (Show)

gridDimensions :: Grid a -> (Int, Int)
gridDimensions grid = (gridWidth grid, gridHeight grid)

instance Functor Grid where
  fmap f (Grid w h c) = Grid w h (fmap f c)

type Coord = (Int, Int)

fromIndex :: Grid a -> Int -> Coord
fromIndex (Grid w _ _) i = (i `mod` w, i `div` w)

toIndex :: Grid a -> Coord -> Int
toIndex (Grid w _ _) (x, y) = y * w + x

neighbors :: Coord -> Grid a -> [(Coord, a)]
neighbors (x, y) g@(Grid w h cs) =
  [ ((x', y'), cs V.! toIndex g (x', y'))
  | x' <- [x - 1 .. x + 1]
  , y' <- [y - 1 .. y + 1]
  , 0 <= x' && x' < w && 0 <= y' && y' < h && (x', y') /= (x, y)
  ]

ifoldGridl :: (a -> Coord -> b -> a) -> a -> Grid b -> a
ifoldGridl f result g =
  V.ifoldl' (\a i b -> f a (fromIndex g i) b) result (gridCells g)

gridToList :: Grid a -> [((Int, Int), a)]
gridToList = ifoldGridl (\lst c e -> (c, e) : lst) []

rotateR :: [[a]] -> [[a]]
rotateR [] = []
rotateR (x : rst) = foldl' (flip (zipWith (:))) (map (: []) x) rst

flipRows :: [[a]] -> [[a]]
flipRows = map reverse

transpose :: [[a]] -> [[a]]
transpose = flipRows . rotateR

gridLookup :: Grid a -> (Int, Int) -> a
gridLookup g@(Grid _ _ v) c = v V.! toIndex g c

infixl 9 <!>
(<!>) :: Grid a -> (Int, Int) -> a
(<!>) = gridLookup
