module Day6 (part1, part2) where

import Util

part1 :: IO ()
part1 =
  solveWithParser "test/Day6/input" parseRaces $
    product . map (\(t, d) -> length $ filter (\c -> c * (t - c) > d) [0 .. t])

part2 :: IO ()
part2 = solveWithParser @Int "test/Day6/input" parseRacesNoSpaces $ \(time, dist) ->
  let
    t' = fromIntegral time :: Double
    d' = fromIntegral dist :: Double
    lowerBound t d = ceiling $ (t - sqrt (t ^^ (2 :: Int) - 4 * d)) / 2
    upperBound t d = ceiling $ (t + sqrt (t ^^ (2 :: Int) - 4 * d)) / 2
   in
    upperBound t' d' - lowerBound t' d'

parseRacesNoSpaces :: Parser (Int, Int)
parseRacesNoSpaces = (,) <$> parseRowNoSpaces "Time:" <* char '\n' <*> parseRowNoSpaces "Distance:"

parseRaces :: Parser [(Int, Int)]
parseRaces = zip <$> parseRow "Time:" <* char '\n' <*> parseRow "Distance:"

parseRow :: String -> Parser [Int]
parseRow tag = string tag *> space *> decimal `sepBy` many (char ' ')

parseRowNoSpaces :: String -> Parser Int
parseRowNoSpaces tag =
  read @Int . concat
    <$> (string tag *> space *> ((:) <$> digitChar <*> many digitChar) `sepBy` many (char ' '))
