module Day8 (part1, part2) where

import Data.Functor (($>))
import Data.List (foldl')
import qualified Data.Map as M
import Util

part1 :: IO ()
part1 = solveWithParser "test/Day8/input" parseNetwork (length . traverseNetwork)

part2 :: IO ()
part2 = solveWithParser "test/Day8/input" parseNetwork (foldl' lcm 1 . map length . traverseConcurrentNetwork)

data Network = Network
  { directions :: [Direction]
  , nodes :: M.Map String (String, String)
  }
  deriving (Show)

data Direction = L | R deriving (Show, Eq)

traverseNetwork :: Network -> [String]
traverseNetwork (Network dirs nodeMap) =
  let
    go (next : rest) node =
      if node == "ZZZ"
        then []
        else
          let
            (lNode, rNode) = nodeMap M.! node
           in
            case next of
              L -> lNode : go rest lNode
              R -> rNode : go rest rNode
   in
    go (cycle dirs) "AAA"

traverseConcurrentNetwork :: Network -> [[String]]
traverseConcurrentNetwork (Network dirs nodeMap) =
  let
    endsWith c k = last k == c
    cycleNetwork (next : rest) node =
      if endsWith 'Z' node
        then []
        else
          let
            (lNode, rNode) = nodeMap M.! node
           in
            case next of
              L -> lNode : cycleNetwork rest lNode
              R -> rNode : cycleNetwork rest rNode
   in
    map (cycleNetwork (cycle dirs)) (filter (endsWith 'A') (M.keys nodeMap))

parseNetwork :: Parser Network
parseNetwork =
  (\ins ns -> Network ins (M.fromList ns))
    <$> parseInstructions
    <* string "\n\n"
    <*> parseNode
    `sepEndBy` char '\n'

parseInstructions :: Parser [Direction]
parseInstructions = many ((char 'R' $> R) <|> (char 'L' $> L))

parseNode :: Parser (String, (String, String))
parseNode =
  (\currNode lNode rNode -> (currNode, (lNode, rNode)))
    <$> many alphaNumChar
    <* string " = "
    <* char '('
    <*> many alphaNumChar
    <* string ", "
    <*> many alphaNumChar
    <* char ')'
