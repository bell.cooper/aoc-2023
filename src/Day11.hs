module Day11 (part1, part2) where

import Util

part1 :: IO ()
part1 = solve "test/Day11/input" $ \input ->
  let universe = expandUniverse input
      galaxies =
        ifoldGridl (\a coord c -> if c == '#' then coord : a else a) [] universe
   in foldr ((+) . uncurry manhattanDistance) 0 . allUniquePairs $ galaxies

part2 :: IO ()
part2 = solve "test/Day11/input" $ \input ->
  let (xExpansions, yExpansions, universe) = massivelyExpandUniverse input
      expansionParameter = 1000000
      galaxies =
        ifoldGridl (\a coord c -> if c == '#' then coord : a else a) [] universe

      superManhattanDistance (x1, y1) (x2, y2) =
        let
          expand s c = length (filter (<= c) s) * (expansionParameter - 1) + c
          (x1', y1') = (expand xExpansions x1, expand yExpansions y1)
          (x2', y2') = (expand xExpansions x2, expand yExpansions y2)
         in
          manhattanDistance (x1', y1') (x2', y2')
   in foldr ((+) . uncurry superManhattanDistance) 0 . allUniquePairs $ galaxies

allUniquePairs :: [a] -> [(a, a)]
allUniquePairs [] = []
allUniquePairs (x : xs) = [(x, y) | y <- xs] ++ allUniquePairs xs

manhattanDistance :: Coord -> Coord -> Int
manhattanDistance (x1, y1) (x2, y2) = abs (x1 - x2) + abs (y1 - y2)

massivelyExpandUniverse :: String -> ([Int], [Int], Grid Char)
massivelyExpandUniverse universe =
  let
    countExpansions =
      foldr (\(i, row) s -> if all (== '.') row then i : s else s) [] . zip [0 ..]
    uLines = lines universe
   in
    (countExpansions (transpose uLines), countExpansions uLines, toGrid universe)

expandUniverse :: String -> Grid Char
expandUniverse universe =
  let
    expandRows =
      concatMap (\row -> if all (== '.') row then [row, row] else [row])
   in
    toGrid . unlines . transpose . expandRows . transpose . expandRows . lines $ universe
