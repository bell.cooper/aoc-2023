module Day12 (part1) where

import Data.List (group)
import Util

part1 :: IO ()
part1 = solveLinesWith "test/Day12/input" parseLine $ \ls ->
  let
    toPerm c =
      case c of
        '#' -> [1]
        '.' -> [0]
        '?' -> [0, 1]
        _ -> error "Bad char"

    countMatchingPerms springPatterns expectedSpringGroups =
      length
        . filter (== expectedSpringGroups)
        . map (filter (> 0) . map sum . group)
        . permutations
        . map toPerm
        $ springPatterns
   in
    sum $ map (uncurry countMatchingPerms) ls

permutations :: [[Int]] -> [[Int]]
permutations [] = [[]]
permutations (x : xs) = concatMap (\h -> map (h :) (permutations xs)) x

parseLine :: String -> (String, [Int])
parseLine input =
  let [arr, numLst] = words input
   in (arr, map (read @Int) . words $ map (\c -> if c == ',' then ' ' else c) numLst)
