{-# LANGUAGE RankNTypes #-}

module Day4 (part1, part2) where

import qualified Control.Monad.ST as ST
import Data.Function (on)
import qualified Data.Map as M
import Data.STRef
import qualified Data.Set as S
import Util

part1 :: IO ()
part1 = solveLinesWithParser @Int "test/Day4/input" parseCard $ \cards ->
  let
    numWins c =
      let l = length (wins c)
       in if l == 0 then 0 else 2 ^ (l - 1)
   in
    sum $ map numWins cards

part2 :: IO ()
part2 = solveLinesWithParser "test/Day4/input" parseCard $ \cards ->
  let
    scratchMap = M.fromList $ map (\c -> (cardId c, c)) cards
    winF f c =
      let
        ns = map (scratchMap M.!) [cardId c + 1 .. cardId c + length (wins c)]
       in
        (c :) . concat <$> traverse f ns
   in
    length $ concatMap (memo winF) cards

memo ::
  (Ord a) =>
  (forall f. Applicative f => (a -> f b) -> a -> f b) ->
  a ->
  b
memo f c = ST.runST $ do
  callMapRef <- newSTRef M.empty
  let
    go c' = do
      callMap <- readSTRef callMapRef
      case M.lookup c' callMap of
        Just rv -> pure rv
        Nothing -> do
          rv <- f go c'
          callMap' <- readSTRef callMapRef
          writeSTRef callMapRef $! M.insert c' rv callMap'
          pure rv
  go c

wins :: Card -> S.Set Int
wins (Card _ w p) = w `S.intersection` p

data Card = Card
  { cardId :: Int
  , winningNumbers :: S.Set Int
  , playedNumbers :: S.Set Int
  }
  deriving (Show)

instance Eq Card where
  (==) = (==) `on` cardId

instance Ord Card where
  compare = compare `on` cardId

parseCard :: Parser Card
parseCard =
  Card <$> parseCardId <*> parseNumSet <* char '|' <* space <*> parseNumSet

parseCardId :: Parser Int
parseCardId = string "Card" *> space *> decimal <* char ':' <* space

parseNumSet :: Parser (S.Set Int)
parseNumSet = S.fromList <$> (decimal `sepEndBy` space)
