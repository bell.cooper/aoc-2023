module Day5 (part1, part2) where

import Data.List (foldl')
import qualified Data.Map as M
import Data.Maybe (mapMaybe)
import Util

data Range = Range
  { fromStart :: Int
  , toStart :: Int
  , rangeValue :: Int
  }
  deriving (Show)

data RangeMap = RangeMap
  { fromMap :: String
  , toMap :: String
  , ranges :: [Range]
  }
  deriving (Show)

type Categories = M.Map String RangeMap

data Seeds = Seeds
  { seeds :: [Int]
  , maps :: Categories
  }
  deriving (Show)

part1 :: IO ()
part1 = solveWithParser "test/Day5/input" parseSeedsAndMaps $ \(Seeds ss ms) ->
  minimum $ map (foldSeeds lookupRange ms) ss

part2 :: IO ()
part2 = solveWithParser "test/Day5/input" parseSeedsAndMaps $ \sd ->
  let
    rangePairs (x : y : rst) = (x, y) : rangePairs rst
    rangePairs _ = []

    pairs = map (\(f, t) -> [(f, f + t - 1)]) (rangePairs (seeds sd))
   in
    minimum . map fst $ concatMap (flattenSeed' (maps sd)) pairs

flattenSeed' :: Categories -> [(Int, Int)] -> [(Int, Int)]
flattenSeed' =
  let
    joinRanges (found, toLook) (Range d s a) =
      let
        toDest (f, t) = (f + d - s, t + d - s)
        r = (s, s + a - 1)
        (fs, ls) =
          foldr
            ( \r' (f, t) ->
                (map toDest (intersectRanges r' r) ++ f, differenceRanges r' r ++ t)
            )
            ([], [])
            toLook
       in
        (fs ++ found, ls)
   in
    foldSeeds
      ( \rs nums ->
          let (fs, ds) = foldl' joinRanges ([], nums) rs
           in fs ++ ds
      )

intersectRanges :: (Int, Int) -> (Int, Int) -> [(Int, Int)]
intersectRanges (f, t) (f', t')
  | t < f' || t' < f = []
  | f' <= f && t <= t' = [(f, t)]
  | f <= f' && t' <= t = [(f', t')]
  | f <= f' && t <= t' = [(f', t)]
  | otherwise = [(f, t')]

differenceRanges :: (Int, Int) -> (Int, Int) -> [(Int, Int)]
differenceRanges (f, t) (f', t')
  | t < f' || t' < f = [(f, t)]
  | f' <= f && t <= t' = []
  | f <= f' && t' <= t = [(f, f' - 1), (t' + 1, t)]
  | f <= f' && t <= t' = [(f, f' - 1)]
  | otherwise = [(t' + 1, t)]

foldSeeds :: ([Range] -> b -> b) -> Categories -> b -> b
foldSeeds f ms n =
  let
    go (from, num) =
      case M.lookup from ms of
        Nothing -> num
        Just (RangeMap _ to rs) -> go (to, f rs num)
   in
    go ("seed", n)

lookupRange :: [Range] -> Int -> Int
lookupRange rs n =
  let
    lookupSingleRange (Range f t r) =
      if t <= n && n < t + r
        then Just $ f + n - t
        else Nothing
   in
    case mapMaybe lookupSingleRange rs of
      [] -> n
      [x] -> x
      _ -> error "Found overlapping ranges"

parseSeedsAndMaps :: Parser Seeds
parseSeedsAndMaps =
  Seeds
    <$> parseSeeds
    <* string "\n\n"
    <*> (M.fromList . map (\m -> (fromMap m, m)) <$> parseMap `sepBy` char '\n')

parseSeeds :: Parser [Int]
parseSeeds = string "seeds: " *> (decimal `sepBy` char ' ')

parseMap :: Parser RangeMap
parseMap =
  RangeMap
    <$> many (anySingleBut '-')
    <* string "-to-"
    <*> many (anySingleBut ' ')
    <* string " map:\n"
    <*> parseRange
    `endBy` char '\n'

parseRange :: Parser Range
parseRange = Range <$> decimal <* char ' ' <*> decimal <* char ' ' <*> decimal
