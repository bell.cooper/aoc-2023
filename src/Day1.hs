module Day1 (part1, part2) where

import Data.Char (isDigit)
import Data.List (find, foldl')
import Data.Maybe (fromJust, fromMaybe)
import Text.Megaparsec
import Util

part1 :: IO ()
part1 =
  let
    calibrationValue ls =
      fmap (read @Int) . (:) <$> find isDigit ls <*> ((: []) <$> find isDigit (reverse ls))
   in
    solveLines "test/Day1/input" $ sum . fromJust . traverse calibrationValue

part2 :: IO ()
part2 =
  let
    calibrationValue ls =
      let
        ns = findAllDigitOrNumberWord ls
        digitString w = fromMaybe w (numFromNumWord w)
       in
        read @Int $ digitString (head ns) <> digitString (last ns)
   in
    solveLines "test/Day1/input" $ sum . map calibrationValue

findAllDigitOrNumberWord :: String -> [String]
findAllDigitOrNumberWord inp =
  if null inp
    then []
    else case parse (parseDigit <|> parseNumberWord) "" inp of
      Left _ -> findAllDigitOrNumberWord (drop 1 inp)
      Right m -> m : findAllDigitOrNumberWord (drop 1 inp)

numbers :: [String]
numbers = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

numFromNumWord :: String -> Maybe String
numFromNumWord w = let numMap = zip numbers (map (show @Int) [1 ..]) in lookup w numMap

parseNumberWord :: Parser String
parseNumberWord = foldl' (\a s -> try (chunk s) <|> a) (chunk $ last numbers) numbers

parseDigit :: Parser String
parseDigit = (: []) <$> satisfy isDigit
